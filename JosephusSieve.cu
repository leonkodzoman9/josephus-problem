//#ifndef __CUDACC__
//#define __CUDACC__
//#include "cuda_runtime.h"
//#include "device_launch_parameters.h"
//#endif
//
//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//
//#include <immintrin.h>
//
//
//
//static constexpr uint32_t N = 33ull;
//static constexpr uint32_t QsPerThread = std::max(32ull, (1ull << N) / (1024ull * 256ull));
//
//static constexpr uint64_t UpperBound = 1ull << N;
//static constexpr uint64_t CombinationCount = 1ull << N;
//
//
//
//__device__
//uint64_t nthSet(uint64_t value, uint64_t bitIndex) {
//    uint32_t index = 0;
//    if (uint32_t p = __popc(value & 0xFFFFFFFFu); p <= bitIndex) {
//        value >>= 32u;
//        index += 32u;
//        bitIndex -= p;
//    }
//    if (uint32_t p = __popc(value & 0xFFFFu); p <= bitIndex) {
//        value >>= 16u;
//        index += 16u;
//        bitIndex -= p;
//    }
//    if (uint32_t p = __popc(value & 0xFFu); p <= bitIndex) {
//        value >>= 8u;
//        index += 8u;
//        bitIndex -= p;
//    }
//    if (uint32_t p = __popc(value & 0xFu); p <= bitIndex) {
//        value >>= 4u;
//        index += 4u;
//        bitIndex -= p;
//    }
//    if (uint32_t p = __popc(value & 0x3u); p <= bitIndex) {
//        value >>= 2u;
//        index += 2u;
//        bitIndex -= p;
//    }
//    if (uint32_t p = __popc(value & 0x1u); p <= bitIndex) {
//        value >>= 1u;
//        index += 1u;
//        bitIndex -= p;
//    }
//    return 1ull << index;
//}
//
//
//
//__global__
//void josephus(uint8_t* occurances) {
//
//    uint32_t q0 = blockDim.x * blockIdx.x + threadIdx.x;
//
//    for (uint32_t qi = 0; qi < QsPerThread; qi++) {
//
//        uint64_t q = q0 + qi * (UpperBound / QsPerThread);
//
//        uint64_t combination = CombinationCount - 1ull;
//        uint64_t index = 0;
//
//        for (uint64_t friendCount = N; friendCount > 0u; friendCount--) {
//            index = (index + q) % friendCount;
//            combination -= nthSet(combination, index);
//            occurances[combination] = 1u;
//        }
//    }
//}
//
//
//
//int main() {
//
//    uint8_t* sieveGpu;
//    cudaMalloc(&sieveGpu, CombinationCount * sizeof(uint8_t));
//
//    std::vector<uint8_t> sieveCpu(CombinationCount, 0);
//
//    cudaMemcpy(sieveGpu, sieveCpu.data(), sieveCpu.size() * sizeof(uint8_t), cudaMemcpyHostToDevice);
//
//    auto t1 = std::chrono::steady_clock::now();
//
//    dim3 threads(256);
//    dim3 blocks(UpperBound / QsPerThread / threads.x + 1);
//    josephus << <blocks, threads >> > (sieveGpu);
//    cudaDeviceSynchronize();
//
//    auto t2 = std::chrono::steady_clock::now();
//    float elapsedSeconds = (t2 - t1).count() / 1e9;
//
//    cudaMemcpy(sieveCpu.data(), sieveGpu, sieveCpu.size() * sizeof(uint8_t), cudaMemcpyDeviceToHost);
//
//    int count = 0;
//    for (int64_t i = 0; i < CombinationCount; i++) {
//        count += !sieveCpu[i];
//    }
//    
//    printf("N: %d, Count: %d, Time: %f s\n", N, count, elapsedSeconds);
//    printf("%f M q/s\n", UpperBound / 1e6 / elapsedSeconds);
//    
//
//    return 0;
//}