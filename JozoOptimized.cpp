//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//#include <algorithm>
//#include <execution>
//
//#include <immintrin.h>
//
//
//
//constexpr uint64_t ipow(uint64_t n, uint64_t p) {
//
//	uint64_t result = 1;
//	for (int i = 0; i < p; i++) {
//		result *= n;
//	}
//
//	return result;
//}
//
//
//
//static constexpr int UnrollCount = 64;
//static constexpr int N = 21;
//static constexpr int64_t UpperBound = ipow(2, 1.1 * N + 1);
//
//static constexpr int64_t CombinationCount = 1 << N;
//
//
//
//void josephus(std::vector<uint8_t>& occurances) {
//
//	std::array<uint8_t, 64> starts{};
//	for (int64_t i = 0; i < starts.size(); i++) {
//		starts[i] = N - 1 - i;
//	}
//	__m256i _s1 = _mm256_loadu_si256((__m256i*)&starts[0]);
//	__m256i _s2 = _mm256_loadu_si256((__m256i*)&starts[32]);
//
//	std::array<uint64_t, 64> magics{}; 
//	for (int64_t i = 1; i < starts.size(); i++) {
//		magics[i] = UINT64_MAX / i + 1;
//	}
//
//	std::array<std::array<uint8_t, 128>, UnrollCount> positionsArrays{};
//
//	std::array<uint64_t, UnrollCount> qs{};
//	for (int i = 0; i < UnrollCount; i++) {
//		qs[i] = i;
//	}
//
//	for (int64_t q = 0; q <= UpperBound; q += UnrollCount) {
//
//		for (int i = 0; i < UnrollCount; i++) {
//			_mm256_storeu_si256((__m256i*)&positionsArrays[i][0], _s1);
//			_mm256_storeu_si256((__m256i*)&positionsArrays[i][32], _s2);
//		}
//		for (int i = 0; i < UnrollCount; i++) {
//			qs[i] += UnrollCount;
//		}
//
//		std::array<uint64_t, UnrollCount> combinations{};
//		combinations.fill(CombinationCount - 1);
//
//		std::array<int8_t, UnrollCount> indices{};
//		indices.fill(0);
//
//		for (int friendCount = N; friendCount > 0; friendCount--) {
//
//			uint64_t magic = magics[friendCount];
//			for (int i = 0; i < UnrollCount; i++) {
//				uint64_t hi;
//				_mulx_u64((indices[i] + qs[i]) * magic, friendCount, &hi);
//				indices[i] = hi;
//			}
//
//			for (int i = 0; i < UnrollCount; i++) {
//				combinations[i] -= (1ull << positionsArrays[i][indices[i]]);
//			}
//
//			for (int i = 0; i < UnrollCount; i++) {
//				uint8_t* indexToDelete = positionsArrays[i].data() + indices[i];
//				__m256i _p1 = _mm256_loadu_si256((__m256i*)(indexToDelete + 1));
//				__m256i _p2 = _mm256_loadu_si256((__m256i*)(indexToDelete + 1 + 32));
//				_mm256_storeu_si256((__m256i*)(indexToDelete), _p1);
//				_mm256_storeu_si256((__m256i*)(indexToDelete + 32), _p2);
//			}
//			
//			for (int i = 0; i < UnrollCount; i++) {
//				occurances[combinations[i]] = 1;
//			}
//		}
//	}
//}
//
//int main() {
//
//	std::vector<uint8_t> occurances(CombinationCount);
//
//	auto t1 = std::chrono::steady_clock::now();
//	josephus(occurances);
//	auto t2 = std::chrono::steady_clock::now();
//
//	std::array<int, N> counts;
//	counts.fill(0);
//
//	int count = 0;
//	for (int64_t i = 0; i < CombinationCount - 1; i++) {
//		count += !occurances[i];
//		if (!occurances[i]) {
//			counts[_mm_popcnt_u64(i)]++;
//		}
//	}
//
//	printf("N: %d, Count: %d, Time: %f s\n", N, count, (t2 - t1).count() / 1e9);
//	printf("%f ns/iter\n", (t2 - t1).count() / double(UpperBound * N));
//
//	for (int i = 0; i < N; i++) {
//		if (counts[i] == 0) { continue; }
//		printf("%2d: %d\n", N - i, counts[i]);
//	}
//
//	return 0;
//}
//
//
//
//
//
//
