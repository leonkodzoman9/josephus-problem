//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//#include <algorithm>
//#include <execution>
//#include <bitset>
//#include <numeric>
//#include <execution>
//
//#include <ranges>
//
//#include <immintrin.h>
//
//
//
//struct Interval {
//	uint64_t start;
//	uint64_t end;
//};
//
//void addValueToIntervals(std::vector<Interval>& intervals, uint64_t value) {
//
//	int i = std::lower_bound(intervals.begin(), intervals.end(), value, [](Interval& interval, uint64_t value) { return interval.start <= value; }) - intervals.begin() - 1;
//
//	if (i >= 0) {
//
//		bool isContained = value <= intervals[i].end;
//
//		if (isContained) {
//			return;
//		}
//
//		bool toExtendPrev = value == intervals[i].end + 1;
//		bool toExtendNext = (i + 1 < intervals.size()) && (value == intervals[i + 1].start - 1);
//
//		if (toExtendPrev && toExtendNext) {
//			intervals[i].end = intervals[i + 1].end;
//			intervals.erase(intervals.begin() + i + 1);
//		}
//		else if (toExtendPrev && !toExtendNext) {
//			intervals[i].end++;
//		}
//		else if (!toExtendPrev && toExtendNext) {
//			intervals[i + 1].start--;
//		}
//		else if (!toExtendPrev && !toExtendNext) {
//			intervals.insert(intervals.begin() + i + 1, { .start = value, .end = value });
//		}
//
//		return;
//	}
//
//	intervals.insert(intervals.begin(), { .start = value, .end = value });
//}
//
//
//
//constexpr uint64_t lcmUpTo(uint64_t x) {
//
//	uint64_t lcm = 1;
//	for (int i = 1; i <= x; i++) {
//		lcm = std::lcm(lcm, i);
//	}
//
//	return lcm;
//}
//
//static constexpr int N = 18;
//static constexpr int64_t UpperBound = lcmUpTo(N);
//
//static constexpr int64_t CombinationCount = 1 << N;
//
//void josephus(std::vector<uint8_t>& __restrict occurances) {
//
//	std::vector<Interval> intervals;
//
//	std::array<uint64_t, 64> magics;
//	for (int i = 1; i < 64; i++) {
//		magics[i] = 0xFFFFFFFFFFFFFFFF / i + 1;
//	}
//
//	int maxInts = 0;
//
//	for (int64_t q = 0; q < UpperBound; q++) {
//
//		uint64_t combination = CombinationCount - 1;
//		uint64_t index = 0;
//
//		for (int friendCount = N; friendCount > 0; friendCount--) {
//			_mulx_u64((index + q) * magics[friendCount], friendCount, &index);
//			combination -= _pdep_u64(1 << index, combination);
//
//			addValueToIntervals(intervals, combination);
//
//			maxInts = std::max(maxInts, (int)intervals.size());
//		}
//	}
//
//	printf("%lld, (%lld), %d\n", intervals.size(), intervals.size() - 1, maxInts);
//}
//
//int main() {
//
//	std::vector<uint8_t> occurances(CombinationCount);
//
//	auto t1 = std::chrono::steady_clock::now();
//	josephus(occurances);
//	auto t2 = std::chrono::steady_clock::now();
//
//	std::array<int, N> counts;
//	counts.fill(0);
//	
//	int count = 0;
//	for (int64_t i = 0; i < CombinationCount - 1; i++) {
//		count += !occurances[i];
//		if (!occurances[i]) {
//			counts[_mm_popcnt_u64(i)]++;
//		}
//	}
//
//	printf("N: %d, Count: %d, Time: %f s\n", N, count, (t2 - t1).count() / 1e9);
//	printf("%f ns/q\n", (t2 - t1).count() / double(UpperBound));
//	printf("%f Mqs/s\n", double(UpperBound) / (t2 - t1).count() * 1e9 / 1e6);
//	
//	for (int i = 0; i < N; i++) {
//		if (counts[i] == 0) { continue; }
//		printf("%2d: %d\n", N - i, counts[i]);
//	}
//
//	return 0;
//}
//
//
//
//
//
//
