//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//#include <algorithm>
//#include <execution>
//#include <bitset>
//#include <fstream>
//#include <mutex>
//#include <cstring>
//#include <memory>
//#include <utility>
//
//#include <immintrin.h>
//
//#include "Utility.hpp"
//
//
//
//constexpr uint32_t N = 6u;
//
//uint64_t resolveEquations(const std::array<uint8_t, N + 1>& equations) {
//
//	uint64_t scale = 1ull;
//	uint64_t offset = 0ull;
//	for (uint32_t divisor = 1u; divisor < equations.size(); divisor++) {
//		for (uint64_t s = 0; s < divisor; s++) {
//			uint64_t lhs = (offset + scale * s) % divisor;
//			if (lhs == equations[divisor]) {
//				offset += scale * s;
//				scale = std::lcm(scale, divisor);
//				break;
//			}
//		}
//	}
//
//	return offset;
//}
//
//void printBitsetGreen(uint64_t q) {
//	printf("\33[32m%s\33[0m", std::bitset<N>(q).to_string().data());
//}
//void printBitsetRed(uint64_t q) {
//	printf("\33[31m%s\33[0m", std::bitset<N>(q).to_string().data());
//}
//
//std::array<std::vector<uint64_t>, N - 1> solutions;
//void createSolutions(uint64_t q) {
//	uint64_t combination = (1ull << N) - 1;
//	uint64_t index = 0;
//    int i = 0;
//	for (int friendCount = N; friendCount > 1; friendCount--) {
//		index = (index + q) % friendCount;
//        combination -= _pdep_u64(1ull << index, combination);
//        solutions[i++].push_back(combination);
//	}
//}
//
//void iterate(std::array<uint8_t, N + 1>& offsets, int depth) {
//
//	if (depth == N) {
//		createSolutions(resolveEquations(offsets));
//		return;
//	}
//
//	for (int i = 0; i <= depth; i++) {
//
//		bool canUse = true;
//		for (int j = 1; j <= depth; j++) {
//			if ((depth + 1) % j == 0) {
//				if (offsets[j] != i % j) {
//					canUse = false;
//					break;
//				}
//			}
//		}
//		if (!canUse) {
//			continue;
//		}
//		offsets[depth + 1] = i;
//		iterate(offsets, depth + 1);
//	}
//}
//
//uint64_t mirror(uint64_t q) {
//    uint64_t result = 0;
//    for (int i = 0; i < N; i++) {
//        result = (result << 1) | (q & 1);
//        q >>= 1;
//    }
//    return result;
//}
//
//int main() {
//
//	std::array<uint8_t, N + 1> offsets;
//	iterate(offsets, 0);
//
//    for (size_t i = 0; i < solutions.size(); i++) {
//        printf("%6llu | ", i + 1);
//        for (size_t j = 0; j < solutions[i].size() / 2; j++) {
//            if (std::find(solutions[i].begin() + j + 1, solutions[i].end(), solutions[i][j]) != solutions[i].end()) {
//                printBitsetGreen(solutions[i][j]);
//            } else {
//                printBitsetRed(solutions[i][j]);
//            }
//            printf(" ");
//        }
//        printf("\n");
//    }
//
//	return 0;
//}
//
//
//
//
//
//
