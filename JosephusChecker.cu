//#include "cuda_runtime.h"
//#include "device_launch_parameters.h"
//
//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//
//#include <immintrin.h>
//
//#include "Utility.hpp"
//
//
//
//constexpr uint64_t GetUpperBound(uint64_t n) {
//
//    uint64_t result = 1;
//    for (int i = 1; i <= n; i++) {
//        result = std::lcm(result, i);
//    }
//
//    return result;
//}
//constexpr uint64_t GetSetBitCount(uint64_t n) {
//
//    uint64_t result = 0;
//    while (n) {
//        result += n & 1;
//        n >>= 1;
//    }
//
//    return result;
//}
//
//
//static constexpr int N = 36;
//static constexpr int64_t UpperBound = GetUpperBound(N);
//
//static constexpr uint64_t Combination = 0b000000001011111011111111111111111111;
//static constexpr int Popcnt = GetSetBitCount(Combination);
//
//
//
//__global__
//void josephusVerify(int64_t offset) {
//
//    int64_t q = blockDim.x * blockIdx.x + threadIdx.x + offset;
//
//    uint8_t index = 0;
//    uint64_t combination = Combination;
//
//    bool failed = false;
//
//    uint8_t friendCount = N;
//    for (; friendCount > Popcnt; friendCount--) {
//
//        index = (index + q) % friendCount;
//
//        if ((combination >> index) & 1) {
//            failed = true;
//        }
//
//        uint64_t high = combination & (UINT64_MAX << index);
//        uint64_t low = combination & (UINT64_MAX >> (64 - index));
//        combination = (high >> 1) | low;
//    }
//
//    if (friendCount == Popcnt && !failed) {
//        printf("Found: %llu\n", q + 1);
//    }
//}
//
//
//
//int main() {
//
//    auto t1 = std::chrono::steady_clock::now();
//    
//    dim3 threads(1024);
//    dim3 blocks(1024);
//    
//    int64_t offset = 0;
//    int iters = 0;
//    while (offset < UpperBound) {
//        josephusVerify << <blocks, threads >> > (offset);
//    
//        auto tLoop = std::chrono::steady_clock::now();
//    
//        float percent = double(offset) / UpperBound;
//        float elapsed = (tLoop - t1).count() / 1e9;
//    
//        if (++iters % 1000 == 0) {
//            printf("%f %%, ETA: %f s\r", percent * 100.0, elapsed / percent * (1 - percent));
//            //break;
//        }
//        offset += threads.x * blocks.x;
//        cudaDeviceSynchronize();
//    }
//    
//    auto t2 = std::chrono::steady_clock::now();
//    
//    //Utility::PrintCheckingSequency(Combination, 5858627938, N);
//
//    return 0;
//}
//
