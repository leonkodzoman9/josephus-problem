//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//#include <algorithm>
//#include <execution>
//#include <bitset>
//#include <fstream>
//#include <mutex>
//#include <cstring>
//#include <memory>
//#include <utility>
//
//#include <immintrin.h>
//
//#include "Utility.hpp"
//
//
//
//static constexpr uint32_t N = 12;
//
//
//
//static consteval std::array<uint64_t, 64> createLcms() {
//	std::array<uint64_t, 64> lcms{};
//
//	uint64_t scale = 1;
//	for (int i = 0; i < N; i++) {
//		lcms[i] = scale;
//		scale = std::lcm(scale, N - i);
//	}
//	return lcms;
//}
//static consteval std::array<uint64_t, 64> createTops() {
//	std::array<uint64_t, 64> tops{};
//	uint64_t scale = 1;
//	for (int i = 0; i < N; i++) {
//		tops[i] = (N - i) / std::gcd(scale, N - i);
//		scale = std::lcm(scale, N - i);
//	}
//	return tops;
//}
//
//constexpr std::array<uint64_t, 64> Lcms = createLcms();
//constexpr std::array<uint64_t, 64> Tops = createTops();
//
//template <uint32_t Depth>
//constexpr std::optional<uint64_t> getNextOffset(uint64_t e1o, uint64_t e2o) {
//	
//	for (uint64_t i = 0; i < Tops[Depth]; i++) {
//		uint64_t offset = e1o + i * Lcms[Depth];
//		if (offset % (N - Depth) == e2o) {
//			return offset;
//		}
//	}
//
//	return std::nullopt;
//}
//
//template <uint32_t Depth, uint32_t ZeroCount>
//struct Validator {
//	static std::optional<uint64_t> validate(std::array<uint8_t, 64>& zeros, uint64_t combination, uint32_t currentIndex, uint32_t previousIndex, uint64_t offset) {
//
//		// Find how many bits there were between the previous index and the current one
//
//		uint32_t shift = (63 - previousIndex) % 64;
//		uint64_t combo = std::rotl(combination, shift);
//		uint64_t newOffset = std::popcount(_bzhi_u64(combo, (currentIndex + shift) % 64));
//		combination -= 1ull << currentIndex;
//
//		// Calculate the next offset
//
//		std::optional<uint64_t> nextOffset = getNextOffset<Depth>(offset, newOffset);
//		if (!nextOffset.has_value()) {
//			return std::nullopt;
//		}
//		offset = nextOffset.value();
//
//		// If this is the end of the chain of equations return the result
//
//		if (ZeroCount == 0) {
//			return offset;
//		}
//
//		for (uint32_t i = 0; i < ZeroCount; i++) {
//
//			uint32_t zeroIndex = zeros[i];
//			std::swap(zeros[i], zeros[ZeroCount - 1]);
//
//			std::optional<uint64_t> possibleSolution = Validator<Depth + 1, ZeroCount - 1>::validate(zeros, combination, zeroIndex, currentIndex, offset);
//			if (possibleSolution.has_value()) {
//				return possibleSolution;
//			}
//
//			std::swap(zeros[i], zeros[ZeroCount - 1]);
//		}
//
//		return std::nullopt;
//	}
//};
//
//template <uint32_t ZeroCount>
//struct Validator<N, ZeroCount> {
//	static std::optional<uint64_t> validate(std::array<uint8_t, 64>& zeros, uint64_t combination, uint32_t currentIndex, uint32_t previousIndex, uint64_t offset) {
//		return std::nullopt;
//	}
//};
//
//
//
//static std::optional<uint64_t> validate(uint32_t zeroCount, std::array<uint8_t, 64> zeros, uint32_t currentIndex) {
//	
//	uint64_t combination = (1ull << N) - 1;
//
//	switch (zeroCount) {
//	case 1: return Validator<0, 1>::validate(zeros, combination, currentIndex, -1, 0);
//	case 2: return Validator<0, 2>::validate(zeros, combination, currentIndex, -1, 0);
//	case 3: return Validator<0, 3>::validate(zeros, combination, currentIndex, -1, 0);
//	case 4: return Validator<0, 4>::validate(zeros, combination, currentIndex, -1, 0);
//	case 5: return Validator<0, 5>::validate(zeros, combination, currentIndex, -1, 0);
//	case 6: return Validator<0, 6>::validate(zeros, combination, currentIndex, -1, 0);
//	case 7: return Validator<0, 7>::validate(zeros, combination, currentIndex, -1, 0);
//	case 8: return Validator<0, 8>::validate(zeros, combination, currentIndex, -1, 0);
//	case 9: return Validator<0, 9>::validate(zeros, combination, currentIndex, -1, 0);
//	case 10: return Validator<0, 10>::validate(zeros, combination, currentIndex, -1, 0);
//	case 11: return Validator<0, 11>::validate(zeros, combination, currentIndex, -1, 0);
//	case 12: return Validator<0, 12>::validate(zeros, combination, currentIndex, -1, 0);
//	case 13: return Validator<0, 13>::validate(zeros, combination, currentIndex, -1, 0);
//	case 14: return Validator<0, 14>::validate(zeros, combination, currentIndex, -1, 0);
//	case 15: return Validator<0, 15>::validate(zeros, combination, currentIndex, -1, 0);
//	case 16: return Validator<0, 16>::validate(zeros, combination, currentIndex, -1, 0);
//	case 17: return Validator<0, 17>::validate(zeros, combination, currentIndex, -1, 0);
//	case 18: return Validator<0, 18>::validate(zeros, combination, currentIndex, -1, 0);
//	case 19: return Validator<0, 19>::validate(zeros, combination, currentIndex, -1, 0);
//	case 20: return Validator<0, 20>::validate(zeros, combination, currentIndex, -1, 0);
//	case 21: return Validator<0, 21>::validate(zeros, combination, currentIndex, -1, 0);
//	case 22: return Validator<0, 22>::validate(zeros, combination, currentIndex, -1, 0);
//	case 23: return Validator<0, 23>::validate(zeros, combination, currentIndex, -1, 0);
//	case 24: return Validator<0, 24>::validate(zeros, combination, currentIndex, -1, 0);
//	case 25: return Validator<0, 25>::validate(zeros, combination, currentIndex, -1, 0);
//	case 26: return Validator<0, 26>::validate(zeros, combination, currentIndex, -1, 0);
//	case 27: return Validator<0, 27>::validate(zeros, combination, currentIndex, -1, 0);
//	case 28: return Validator<0, 28>::validate(zeros, combination, currentIndex, -1, 0);
//	case 29: return Validator<0, 29>::validate(zeros, combination, currentIndex, -1, 0);
//	case 30: return Validator<0, 30>::validate(zeros, combination, currentIndex, -1, 0);
//	case 31: return Validator<0, 31>::validate(zeros, combination, currentIndex, -1, 0);
//	case 32: return Validator<0, 32>::validate(zeros, combination, currentIndex, -1, 0);
//	case 33: return Validator<0, 33>::validate(zeros, combination, currentIndex, -1, 0);
//	case 34: return Validator<0, 34>::validate(zeros, combination, currentIndex, -1, 0);
//	case 35: return Validator<0, 35>::validate(zeros, combination, currentIndex, -1, 0);
//	case 36: return Validator<0, 36>::validate(zeros, combination, currentIndex, -1, 0);
//	case 37: return Validator<0, 37>::validate(zeros, combination, currentIndex, -1, 0);
//	case 38: return Validator<0, 38>::validate(zeros, combination, currentIndex, -1, 0);
//	case 39: return Validator<0, 39>::validate(zeros, combination, currentIndex, -1, 0);
//	case 40: return Validator<0, 40>::validate(zeros, combination, currentIndex, -1, 0);
//	}
//
//	return std::nullopt;
//}
//
//
//
//int main() {
//
//	std::string path = "Data/raw/" + std::to_string(N) + ".txt";
//
//	printf("Loading raw data\n");
//
//	std::ifstream file(path);
//	int count;
//	file >> count;
//	std::vector<uint64_t> candidates;
//	for (int i = 0; i < count; i++) {
//		uint64_t combination;
//		file >> combination;
//		candidates.push_back(combination);
//	}
//	file.close();
//
//	candidates = { 2171 };
//
//	int valid = 0;
//	int invalid = 0;
//	int wrong = 0;
//
//	std::mutex mutex;
//
//	printf("Starting processing\n");
//
//	auto t1 = std::chrono::steady_clock::now();
//
//	std::vector<uint64_t> combinations;
//	combinations.reserve(candidates.size());
//
//	std::for_each(std::execution::seq, candidates.begin(), candidates.end(),
//		[&](uint64_t& combination) {
//
//			std::array<uint8_t, 64> zeros{};
//			uint32_t zeroCount = 0;
//			for (int i = 0; i < N; i++) {
//				if (((combination >> i) & 1) == 0) {
//					zeros[zeroCount++] = i;
//				}
//			}
//
//			std::optional<uint64_t> result;
//			for (uint32_t i = 0; i < zeroCount; i++) {
//				uint32_t zeroIndex = zeros[i];
//				std::swap(zeros[i], zeros[zeroCount - 1]);
//
//				zeroCount--;
//				result = validate(zeroCount, zeros, zeroIndex);
//				if (result.has_value()) {
//					break;
//				}
//				zeroCount++;
//
//				std::swap(zeros[i], zeros[zeroCount - 1]);
//			}
//
//			bool hasSolution = result.has_value();
//			bool isSolutionValid = hasSolution ? Utility::ValidateSolution(combination, result.value(), N) : false;
//
//			std::lock_guard guard(mutex);
//
//			if (hasSolution && isSolutionValid) {
//				invalid++;
//			}
//			else if (hasSolution && !isSolutionValid) {
//				wrong++;
//			}
//			else {
//				if (combination != (1ull << N) - 1) {
//					valid++;
//					combinations.push_back(combination);
//				}
//				else {
//					invalid++;
//				}
//			}
//
//			printf("(%d/%d/%d/%lld) %6.2f %%\r", valid, invalid, wrong, candidates.size(), (valid + invalid + wrong) * 100.0f / candidates.size());
//		}
//	);
//
//	auto t2 = std::chrono::steady_clock::now();
//
//	printf("(%d/%d/%d/%lld) %6.2f %%\n", valid, invalid, wrong, candidates.size(), (valid + invalid + wrong) * 100.0f / candidates.size());
//
//	printf("Processing done\n");
//	printf("Found %lld valid combinations\n", combinations.size());
//
//	std::ofstream oFile("Data/filtered/" + std::to_string(N) + ".txt");
//	oFile << combinations.size() << "\n";
//	for (uint64_t combination : combinations) {
//		oFile << std::bitset<N>(combination).to_string() << "\n";
//	}
//	oFile.close();
//
//	printf("%f s\n", (t2 - t1).count() / 1e9);
//
//	return 0;
//}
//
//
//
//
//
//
