#include <iostream>	
#include <vector>
#include <array>
#include <chrono>
#include <algorithm>
#include <execution>
#include <bitset>
#include <execution>
#include <fstream>

#include <ranges>

#include <immintrin.h>





std::array<uint64_t, 64> Magics;

template <uint64_t N>
std::vector<uint64_t> josephus() {

	constexpr uint64_t UpperBound = 2ull << N;
	constexpr uint64_t CombinationCount = 1ull << N;

	constexpr uint64_t SieveSize = 1ull << (std::min(N, 37ull));
	constexpr uint64_t SieveProgressGranularity = 1ull << std::min(24ull, N);
	constexpr int SegmentCount = CombinationCount / SieveSize;

	uint64_t Progress = 0;
	std::mutex Mutex;

	printf("Starting sieve for N: %lld\n", N);

	std::vector<bool> sieve(SieveSize);
	std::vector<uint64_t> combinations;
	for (int i = 0; i < SegmentCount; i++) {

		printf("Starting sieve segment %d/%d\n", i + 1, SegmentCount);

		auto t1 = std::chrono::steady_clock::now();

		uint64_t combinationStart = i * SieveSize;
		uint64_t combinationEnd = (i + 1) * SieveSize;
		auto range = std::views::common(std::views::iota(0ull, UpperBound));
		std::for_each(std::execution::par_unseq, range.begin(), range.end(),
			[&](uint64_t q) {

				uint64_t combination = CombinationCount - 1;
				uint64_t index = 0;

				for (int friendCount = N; friendCount > 0; friendCount--) {
					_mulx_u64((index + q) * Magics[friendCount], friendCount, &index);
					combination -= _pdep_u64(1ull << index, combination);

					if (combination >= combinationStart && combination < combinationEnd) {
						sieve[combination - combinationStart] = 1;
					}
				}

				if ((q + 1) % SieveProgressGranularity == 0) {

					auto t2 = std::chrono::steady_clock::now();

					uint64_t progress = 0;
					{
						std::lock_guard guard(Mutex);
						Progress += SieveProgressGranularity;
						progress = Progress;
					}

					float elapsed = (t2 - t1).count() / 1e9;
					t1 = t2;

					printf("Sieve %d/%d: %6.2f %%, %f seconds\n", i + 1, SegmentCount, 100.0 * progress / (UpperBound - 1), elapsed);
				}
			}
		);

		printf("Processing sieve\n");

		for (uint64_t j = 0; j < SieveSize; j++) {
			if (!sieve[j]) {
				combinations.push_back(i * SieveSize + j);
			}
		}
	}

	return combinations;
}

std::vector<uint64_t> josephus(uint64_t n) {

	if (n == 1) { return josephus< 1>(); }
	if (n == 2) { return josephus< 2>(); }
	if (n == 3) { return josephus< 3>(); }
	if (n == 4) { return josephus< 4>(); }
	if (n == 5) { return josephus< 5>(); }
	if (n == 6) { return josephus< 6>(); }
	if (n == 7) { return josephus< 7>(); }
	if (n == 8) { return josephus< 8>(); }
	if (n == 9) { return josephus< 9>(); }
	if (n == 10) { return josephus<10>(); }
	if (n == 11) { return josephus<11>(); }
	if (n == 12) { return josephus<12>(); }
	if (n == 13) { return josephus<13>(); }
	if (n == 14) { return josephus<14>(); }
	if (n == 15) { return josephus<15>(); }
	if (n == 16) { return josephus<16>(); }
	if (n == 17) { return josephus<17>(); }
	if (n == 18) { return josephus<18>(); }
	if (n == 19) { return josephus<19>(); }
	if (n == 20) { return josephus<20>(); }
	if (n == 21) { return josephus<21>(); }
	if (n == 22) { return josephus<22>(); }
	if (n == 23) { return josephus<23>(); }
	if (n == 24) { return josephus<24>(); }
	if (n == 25) { return josephus<25>(); }
	if (n == 26) { return josephus<26>(); }
	if (n == 27) { return josephus<27>(); }
	if (n == 28) { return josephus<28>(); }
	if (n == 29) { return josephus<29>(); }
	if (n == 30) { return josephus<30>(); }
	if (n == 31) { return josephus<31>(); }
	if (n == 32) { return josephus<32>(); }
	if (n == 33) { return josephus<33>(); }
	if (n == 34) { return josephus<34>(); }
	if (n == 35) { return josephus<35>(); }
	if (n == 36) { return josephus<36>(); }

	return {};
}

int main() {

	for (int i = 1; i < 64; i++) {
		Magics[i] = 0xFFFFFFFFFFFFFFFF / i + 1;
	}

	for (int n = 33; n <= 33; n++) {
		auto t1 = std::chrono::steady_clock::now();
		std::vector<uint64_t> combinations = josephus(n);
		auto t2 = std::chrono::steady_clock::now();

		float elapsed = (t2 - t1).count() / 1e9;

		printf("Done with sieve, storing to disk\n");

		std::ofstream oFile("Data/raw/" + std::to_string(n) + ".txt");

		oFile << combinations.size() << "\n";
		for (uint64_t combination : combinations) {
			oFile << combination << "\n";
		}
		oFile.close();

		printf("Stored %lld combinations to disk\n", combinations.size());
		printf("Total time: %f seconds\n", elapsed);
	}

	return 0;
}






