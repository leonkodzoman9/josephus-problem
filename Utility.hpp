#pragma once

#include <iostream>
#include <array>



namespace Utility {

	static void PrintCombination(uint64_t combination, int n, int highlightIndex = -1) {

		std::string digits;
		for (int i = 0; i < n; i++) {
			digits = (combination & 1 ? '1' : '0') + digits;
			combination >>= 1;
		}

		for (int i = 0; i < digits.size(); i++) {

			if (n - i - 1 == highlightIndex) {
				if (digits[i] == '1') {
					printf("\x1B[102m\x1B[30m1\033[0m");
				}
				else {
					printf("\x1B[101m\x1B[30m0\033[0m");
				}
			}
			else {
				if (digits[i] == '1') {
					printf("\x1B[32m1\033[0m");
				}
				else {
					printf("\x1B[31m0\033[0m");
				}
			}
		}
		printf("\n");

	}

	static void PrintCheckingSequence(uint64_t combination, uint64_t q, int n) {

		std::array<uint8_t, 128> positions{};
		for (int64_t i = 0; i < positions.size(); i++) {
			positions[i] = i;
		}

		int8_t index = 0;

		printf("   ");
		Utility::PrintCombination(combination, n);

		for (int friendCount = n; friendCount > _mm_popcnt_u64(combination); friendCount--) {

			index = (index + q) % friendCount;

			uint64_t combinationBeforeUpdate = combination;
			combination &= ~(1ull << positions[index]);

			printf("%2d ", friendCount);
			Utility::PrintCombination(combination, n, positions[index]);

			if (combination != combinationBeforeUpdate) {
				printf("Failure!\n");
				return;
			}

			uint8_t* indexToDelete = positions.data() + index;
			std::memcpy(indexToDelete, indexToDelete + 1, 64);
		}

		printf("Success!\n");
	}

	static bool ValidateSolution(uint64_t combination, uint64_t q, int n) {

		std::array<uint8_t, 128> positions{};
		for (int64_t i = 0; i < positions.size(); i++) {
			positions[i] = i;
		}

		int8_t index = 0;

		for (int friendCount = n; friendCount > _mm_popcnt_u64(combination); friendCount--) {

			index = (index + q) % friendCount;

			uint64_t combinationBeforeUpdate = combination;
			combination &= ~(1ull << positions[index]);

			if (combination != combinationBeforeUpdate) {
				return false;
			}

			uint8_t* indexToDelete = positions.data() + index;
			std::memcpy(indexToDelete, indexToDelete + 1, 64);
		}

		return true;
	}
}
