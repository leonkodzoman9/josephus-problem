//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//#include <algorithm>
//#include <execution>
//#include <bitset>
//#include <fstream>
//#include <mutex>
//#include <cstring>
//#include <memory>
//#include <utility>
//
//#include <immintrin.h>
//
//#include "Utility.hpp"
//
//
//
//template <uint32_t Number>
//consteval uint32_t DivisorCount() {
//	uint32_t count = 0u;
//	for (uint32_t i = 2u; i <= Number; i++) {
//		if (Number % i == 0u) {
//			count++;
//		}
//	}
//	return count;
//}
//
//template <uint32_t Number>
//consteval std::array<uint32_t, DivisorCount<Number>()> DivisorsOf() {
//	std::array<uint32_t, DivisorCount<Number>()> divisors{};
//	uint32_t count = 0u;
//	for (uint32_t i = 2u; i <= Number; i++) {
//		if (Number % i == 0u) {
//			divisors[count++] = i;
//		}
//	}
//	return divisors;
//}
//
//template <uint32_t Low, uint32_t High, uint32_t Divisor>
//consteval uint32_t CountDivisors() {
//	uint32_t count = 0u;
//	for (uint32_t n = Low; n <= High; n++) {
//		if (n % Divisor == 0u) {
//			count++;
//		}
//	}
//	return count;
//}
//
//template <uint32_t Number>
//consteval uint32_t RoundToNearestMultipleOf16() {
//	if (Number <= 16u) {
//		return 16u;
//	}
//	if (Number <= 32u) {
//		return 32u;
//	}
//	if (Number <= 48u) {
//		return 48u;
//	}
//	return 64u;
//}
//
//
//
//constexpr uint32_t N = 36u;
//
//
//
//struct EquationSet {
//	std::array<uint8_t, RoundToNearestMultipleOf16<N + 1u>()> remainders;
//	EquationSet() { remainders.fill(UINT8_MAX); }
//};
//
//template <uint32_t Depth, uint32_t Index>
//bool process(EquationSet& equations, uint64_t offset) {
//
//	constexpr uint32_t divisor = DivisorsOf<N - Depth>()[Index];
//	
//	if constexpr (CountDivisors<N - Depth, N, divisor>() == 1u) {
//		equations.remainders[divisor] = offset % divisor;
//	}
//	else if constexpr (CountDivisors<N - Depth, N, divisor>() > 1u) {
//		if (equations.remainders[divisor] != offset % divisor) {
//			return false;
//		}
//	}
//	
//	if constexpr (Index > 0u) {
//		return process<Depth, Index - 1u>(equations, offset);
//	}
//
//	return true;
//}
//
//template <uint32_t Depth>
//constexpr bool processEquations(EquationSet& equations, uint64_t offset) {
//	if constexpr (DivisorCount<N - Depth>() == 0u) {
//		return true;
//	}
//	else {
//		return process<Depth, DivisorCount<N - Depth>() - 1u>(equations, offset);
//	}
//}
//
//template <uint32_t Depth, uint32_t ZeroCount>
//EquationSet* validate(uint64_t zeroMask, uint64_t combination, uint32_t currentIndex, EquationSet equations) {
//
//	uint64_t newOffset = std::popcount(_bzhi_u64(combination, currentIndex));
//	if (!processEquations<Depth>(equations, newOffset)) {
//		return nullptr;
//	}
//
//	if constexpr (ZeroCount == 0u) {
//		EquationSet* solution = new EquationSet;
//		*solution = equations;
//		return solution;
//	}
//
//	if constexpr (ZeroCount > 0u && Depth < N) {
//
//		combination = std::rotr(combination, currentIndex) - 1u;
//		zeroMask = std::rotr(zeroMask, currentIndex);
//
//		for (uint32_t i = 0u; i < ZeroCount; i++) {
//
//			uint64_t targetBit = _pdep_u64(1ull << i, zeroMask);
//			uint64_t zeroIndex = std::countr_zero(targetBit);
//			uint64_t newZeroMask = zeroMask - targetBit;
//
//			EquationSet* possibleSolution = validate<Depth + 1u, ZeroCount - 1u>(newZeroMask, combination, zeroIndex, equations);
//			if (possibleSolution != nullptr) {
//				return possibleSolution;
//			}
//		}
//	}
//
//	return nullptr;
//}
//
//EquationSet* validate(uint32_t zeroCount, uint64_t zeroMask, uint32_t currentIndex) {
//	switch (zeroCount) {
//	case 1: return validate<0, 1>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 2: return validate<0, 2>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 3: return validate<0, 3>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 4: return validate<0, 4>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 5: return validate<0, 5>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 6: return validate<0, 6>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 7: return validate<0, 7>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 8: return validate<0, 8>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 9: return validate<0, 9>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 10: return validate<0, 10>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 11: return validate<0, 11>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 12: return validate<0, 12>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 13: return validate<0, 13>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 14: return validate<0, 14>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 15: return validate<0, 15>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 16: return validate<0, 16>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 17: return validate<0, 17>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 18: return validate<0, 18>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 19: return validate<0, 19>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 20: return validate<0, 20>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 21: return validate<0, 21>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 22: return validate<0, 22>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 23: return validate<0, 23>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 24: return validate<0, 24>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 25: return validate<0, 25>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 26: return validate<0, 26>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 27: return validate<0, 27>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 28: return validate<0, 28>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 29: return validate<0, 29>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 30: return validate<0, 30>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 31: return validate<0, 31>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 32: return validate<0, 32>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 33: return validate<0, 33>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 34: return validate<0, 34>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 35: return validate<0, 35>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 36: return validate<0, 36>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 37: return validate<0, 37>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 38: return validate<0, 38>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 39: return validate<0, 39>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	case 40: return validate<0, 40>(zeroMask, (1ull << N) - 1u, currentIndex, EquationSet());
//	}
//	return nullptr;
//}
//
//
//
//uint64_t resolveEquations(EquationSet equations) {
//
//	uint64_t scale = 1ull;
//	uint64_t offset = 0ull;
//	for (uint32_t divisor = 0u; divisor < equations.remainders.size(); divisor++) {
//		if (equations.remainders[divisor] == UINT8_MAX) { continue; }
//		for (uint64_t s = 0; s < divisor; s++) {
//			if ((offset + scale * s) % divisor == equations.remainders[divisor]) {
//				offset += scale * s;
//				scale = std::lcm(scale, divisor);
//				break;
//			}
//		}
//	}
//
//	return offset;
//}
//
//
//
//int main() {
//
//	std::string path = "Data/raw/" + std::to_string(N) + ".txt";
//
//	printf("Loading raw data\n");
//
//	std::ifstream file(path);
//	int count;
//	file >> count;
//	std::vector<uint64_t> results;
//	std::vector<uint64_t> combinations;
//	for (int i = 0; i < count; i++) {
//		uint64_t combination;
//		file >> combination;
//		results.push_back(combination);
//		combinations.push_back(combination);
//	}
//	file.close();
//
//	printf("Starting processing\n");
//
//	auto t1 = std::chrono::steady_clock::now();
//
//	std::for_each(std::execution::seq, combinations.begin(), combinations.end(),
//		[&](uint64_t& combination) {
//			uint64_t index = (&combination - &combinations.front());
//
//			/*uint64_t mirror = 0ull;
//			uint64_t orig = combination;
//			for (int i = 0; i < N; i++) {
//				mirror = (mirror << 1) + (orig & 1);
//				orig >>= 1;
//			}
//
//			auto iter = std::find(combinations.begin(), combinations.end(), mirror);
//			if (iter == combinations.end()) {
//				results[index] = 0;
//				return;
//			}*/
//
//			uint64_t zeroMask = (~combination) & ((1ull << N) - 1);
//			uint64_t zeroCount = std::popcount(zeroMask);
//		
//			EquationSet* result = nullptr;
//			for (uint32_t i = 0; i < zeroCount; i++) {
//
//				uint64_t zeroIndex = _tzcnt_u64(_pdep_u64(1ull << i, zeroMask));
//				uint64_t newZeroMask = zeroMask - (1ull << zeroIndex);
//
//				result = validate(zeroCount - 1, newZeroMask, zeroIndex);
//				if (result) {
//					break;
//				}
//			}
//
//			if (result != nullptr) {
//				results[index] = resolveEquations(*result);
//				delete result;
//			}
//			else if (zeroCount == 0) {
//				results[index] = 0;
//			}
//			else {
//				results[index] = UINT64_MAX;
//			}
//			
//			if (index % 100 == 0) {
//				printf("%6.2f %%\r", index * 100.0f / combinations.size());
//			}
//		}
//	);
//
//	auto t2 = std::chrono::steady_clock::now();
//
//	printf("%6.2f %%\n", 100.0f);
//
//	printf("Processing done\n");
//
//	uint64_t nonJosephusCount = 0ull;
//	uint64_t josephusCountValid = 0ull;
//	uint64_t josephusCountInvalid = 0ull;
//
//	for (uint64_t i = 0; i < results.size(); i++) {
//
//		uint64_t result = results[i];
//		uint64_t combination = combinations[i];
//
//		if (result == UINT64_MAX) {
//			nonJosephusCount++;
//		}
//		else if (Utility::ValidateSolution(combination, result, N)) {
//			josephusCountValid++;
//		}
//		else {
//			josephusCountInvalid++;
//		}
//	}
//
//	printf("(nj, jv, ji, t)(%llu, %llu, %llu, %llu)\n", nonJosephusCount, josephusCountValid, josephusCountInvalid, combinations.size());
//
//	/*std::ofstream oFile("Data/filtered/" + std::to_string(N) + ".txt");
//	oFile << combinations.size() << "\n";
//	for (uint64_t combination : combinations) {
//		oFile << std::bitset<N>(combination).to_string() << "\n";
//	}
//	oFile.close();*/
//
//	printf("%f s\n", (t2 - t1).count() / 1e9);
//
//	return 0;
//}
//
//
//
//
//
//
