//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <atomic>
//#include <thread>
//
//#include <immintrin.h>
//#include <intrin.h>
//
//
//
//constexpr uint64_t ipow(uint64_t n, uint64_t p) {
//
//	uint64_t result = 1;
//	for (int i = 0; i < p; i++) {
//		result *= n;
//	}
//
//	return result;
//}
//
//
//
//static constexpr int UnrollCount = 128;
//static constexpr int N = 27;
//static constexpr int64_t UpperBound = ipow(2, 1.1 * N + 1);
//
//static constexpr int64_t CombinationCount = 1 << N;
//
//
//
//struct alignas(64) AlignedAtomicBool {
//	std::atomic_bool value;
//};
//
//AlignedAtomicBool Running = { false };
//AlignedAtomicBool CanStoreToMemory = { false };
//AlignedAtomicBool CanStoreToStoreBuffer = { true };
//std::array<uint64_t, N* UnrollCount> StoreBuffer;
//
//
//
//void josephus() {
//
//	std::array<uint8_t, 64> starts{};
//	for (int64_t i = 0; i < starts.size(); i++) {
//		starts[i] = N - 1 - i;
//	}
//	__m256i _s1 = _mm256_loadu_si256((__m256i*) & starts[0]);
//	__m256i _s2 = _mm256_loadu_si256((__m256i*) & starts[32]);
//
//	std::array<uint64_t, 64> magics{};
//	for (int64_t i = 1; i < starts.size(); i++) {
//		magics[i] = UINT64_MAX / i + 1;
//	}
//
//	std::array<std::array<uint8_t, 128>, UnrollCount> positionsArrays{};
//	std::array<uint64_t, N* UnrollCount> indicesToStore{};
//
//	std::array<uint64_t, UnrollCount> qs{};
//	for (int i = 0; i < UnrollCount; i++) {
//		qs[i] = i;
//	}
//
//	for (int64_t q = 0; q <= UpperBound; q += UnrollCount) {
//
//		for (int i = 0; i < UnrollCount; i++) {
//			_mm256_storeu_si256((__m256i*) & positionsArrays[i][0], _s1);
//			_mm256_storeu_si256((__m256i*) & positionsArrays[i][32], _s2);
//		}
//		for (int i = 0; i < UnrollCount; i++) {
//			qs[i] += UnrollCount;
//		}
//
//		std::array<uint64_t, UnrollCount> combinations{};
//		combinations.fill(CombinationCount - 1);
//
//		std::array<uint64_t, UnrollCount> indices{};
//		indices.fill(0);
//
//		int offset = 0;
//		for (uint64_t friendCount = N; friendCount > 0; friendCount--) {
//
//			uint64_t magic = magics[friendCount];
//
//			for (int i = 0; i < UnrollCount; i++) {
//				indices[i] = __umulh((indices[i] + qs[i]) * magic, friendCount);
//			}
//
//			for (int i = 0; i < UnrollCount; i++) {
//				combinations[i] -= (1ull << positionsArrays[i][indices[i]]);
//			}
//
//			for (int i = 0; i < UnrollCount; i++) {
//				uint8_t* indexToDelete = positionsArrays[i].data() + indices[i];
//				__m256i _p1 = _mm256_loadu_si256((__m256i*)(indexToDelete + 1));
//				__m256i _p2 = _mm256_loadu_si256((__m256i*)(indexToDelete + 1 + 32));
//				_mm256_storeu_si256((__m256i*)(indexToDelete), _p1);
//				_mm256_storeu_si256((__m256i*)(indexToDelete + 32), _p2);
//			}
//
//			for (int i = 0; i < UnrollCount; i += 4) {
//				__m256i _v = _mm256_loadu_si256((__m256i*)(&combinations[i]));
//				_mm256_storeu_si256((__m256i*)(&indicesToStore[i + offset]), _v);
//			}
//
//			offset += UnrollCount;
//		}
//
//		while (!CanStoreToStoreBuffer.value) {}
//		std::copy(indicesToStore.begin(), indicesToStore.end(), StoreBuffer.begin());
//		CanStoreToStoreBuffer.value = false;
//		CanStoreToMemory.value = true;
//	}
//}
//
//int main() {
//
//	std::vector<uint8_t> occurances(CombinationCount);
//
//	Running.value = true;
//	std::thread storeThread = std::thread([&]() {
//
//		while (Running.value) {
//
//			while (!CanStoreToMemory.value) {}
//
//			if (!Running.value) {
//				break;
//			}
//
//			for (int i = 0; i < StoreBuffer.size(); i++) {
//				occurances[StoreBuffer[i]] = 1;
//			}
//
//			CanStoreToMemory.value = false;
//			CanStoreToStoreBuffer.value = true;
//		}
//		});
//
//	auto t1 = std::chrono::steady_clock::now();
//	josephus();
//	auto t2 = std::chrono::steady_clock::now();
//
//	CanStoreToMemory.value = true;
//	Running.value = false;
//	storeThread.join();
//
//	int count = 0;
//	for (int64_t i = 0; i < CombinationCount - 1; i++) {
//		count += !occurances[i];
//	}
//
//	printf("N: %d, Count: %d, Time: %f s\n", N, count, (t2 - t1).count() / 1e9);
//	printf("%f ns/iter\n", (t2 - t1).count() / double(UpperBound * N));
//
//	return 0;
//}
//
//
//
//
//
//
