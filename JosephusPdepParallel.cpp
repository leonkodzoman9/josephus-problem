//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//#include <algorithm>
//#include <numeric>
//#include <thread>
//#include <mutex>
//#include <condition_variable>
//
//#include <immintrin.h>
//
//
//
//constexpr uint64_t lcmUpTo(uint64_t x) {
//
//	uint64_t lcm = 1;
//	for (int i = 1; i <= x; i++) {
//		lcm = std::lcm(lcm, i);
//	}
//
//	return lcm;
//}
//
//static constexpr int N = 24;
//static constexpr int64_t UpperBound = lcmUpTo(N);
//static constexpr int64_t CombinationCount = 1 << N;
//
//
//
//static constexpr int ThreadCount = 32;
//static constexpr int QsPerTask = 1024 * 1024 * 32 / N;
//
//bool Running = false;
//std::mutex Mutex;
//std::condition_variable ConditionMain;
//std::condition_variable ConditionJosephus;
//
//struct JosephusTask {
//	uint64_t qStart;
//	std::vector<uint64_t> combinations;
//};
//std::vector<JosephusTask> PendingTasks;
//std::vector<JosephusTask> CompletedTasks;
//
//void josephus() {
//
//	std::vector<uint64_t> combinations;
//
//	while (true) {
//
//		JosephusTask task;
//		{
//			std::unique_lock lock(Mutex);
//			ConditionJosephus.wait(lock, [&]() { return !Running || (PendingTasks.size() > 0 && CompletedTasks.size() < 20); });
//
//			if (!Running) {
//				return;
//			}
//
//			task = std::move(PendingTasks.back());
//			PendingTasks.pop_back();
//		}
//		
//		task.combinations.reserve(QsPerTask * N);
//
//		std::array<uint64_t, 64> magics;
//		for (int i = 1; i < 64; i++) {
//			magics[i] = 0xFFFFFFFFFFFFFFFF / i + 1;
//		}
//
//		for (int64_t q = task.qStart; q < task.qStart + QsPerTask; q++) {
//
//			uint64_t combination = CombinationCount - 1;
//			uint64_t index = 0;
//
//			for (int friendCount = N; friendCount > 0; friendCount--) {
//				_mulx_u64((index + q) * magics[friendCount], friendCount, &index);
//				combination -= _pdep_u64(1 << index, combination);
//				task.combinations.push_back(combination);
//			}
//		}
//
//		{
//			std::lock_guard guard(Mutex);
//			CompletedTasks.emplace_back(std::move(task));
//		}
//		ConditionMain.notify_all();
//	}
//
//	
//}
//
//
//int main() {
//
//	std::vector<uint8_t> occurances(CombinationCount);
//
//	for (uint64_t q = 0; q < UpperBound; q += QsPerTask) {
//		JosephusTask task;
//		task.qStart = q;
//		PendingTasks.push_back(task);
//	}
//	int64_t totalTasks = PendingTasks.size();
//	int64_t tasksRemaining = PendingTasks.size();
//
//	Running = true;
//
//	auto t1 = std::chrono::steady_clock::now();
//
//	std::vector<std::thread> threads(ThreadCount);
//	for (int i = 0; i < ThreadCount; i++) {
//		threads[i] = std::thread(josephus);
//	}
//
//	while (true) {
//
//		JosephusTask task;
//		{
//			std::unique_lock lock(Mutex);
//			ConditionMain.wait(lock, [&]() { return CompletedTasks.size() > 0; });
//
//			task = std::move(CompletedTasks.back());
//			CompletedTasks.pop_back();
//
//			if (tasksRemaining % 10 == 0) {
//				printf("%f, (%d)\n", 100.0f - 100.0f * float(tasksRemaining) / float(totalTasks), tasksRemaining);
//			}
//		}
//
//		for (uint64_t combination : task.combinations) {
//			occurances[combination] = 1;
//		}
//
//		ConditionJosephus.notify_all();
//		
//		if (--tasksRemaining <= 0) {
//			break;
//		}
//		std::lock_guard guard(Mutex);
//		if (PendingTasks.size() > 0) {
//			PendingTasks.back().combinations = std::move(task.combinations);
//		}
//	}
//
//	{
//		std::lock_guard guard(Mutex);
//		Running = false;
//	}
//	ConditionJosephus.notify_all();
//
//	for (int i = 0; i < ThreadCount; i++) {
//		if (threads[i].joinable()) {
//			threads[i].join();
//		}
//	}
//
//	auto t2 = std::chrono::steady_clock::now();
//
//	std::array<int, N> counts;
//	counts.fill(0);
//
//	int count = 0;
//	for (int64_t i = 0; i < CombinationCount - 1; i++) {
//		count += !occurances[i];
//		if (!occurances[i]) {
//			counts[_mm_popcnt_u64(i)]++;
//		}
//	}
//
//	printf("N: %d, Count: %d, Time: %f s\n", N, count, (t2 - t1).count() / 1e9);
//	printf("%f ns/q\n", (t2 - t1).count() / double(UpperBound));
//	printf("%f Mqs/s\n", double(UpperBound) / (t2 - t1).count() * 1e9 / 1e6);
//
//	for (int i = 0; i < N; i++) {
//		if (counts[i] == 0) { continue; }
//		printf("%2d: %d\n", N - i, counts[i]);
//	}
//
//	return 0;
//}
//
//
//
//
//
//
