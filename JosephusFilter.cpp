//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//#include <algorithm>
//#include <execution>
//#include <bitset>
//#include <numeric>
//#include <execution>
//#include <fstream>
//#include <mutex>
//#include <unordered_set>
//#include <unordered_map>
//
//#include <ranges>
//
//#include <immintrin.h>
//
//#include "Utility.hpp"
//
//
//
//static constexpr int N = 28;
//
//
//
//std::array<uint64_t, 64> Magics;
//
//
//
//uint64_t fancyMod(uint64_t x, uint64_t m) {
//	_mulx_u64(x * Magics[m], m, &x);
//	return x;
//}
//
//
//
//struct Equation {
//	uint64_t offset;
//	uint64_t scale;
//};
//
//uint64_t egcd(uint64_t a, uint64_t b, int64_t& x, int64_t& y) {
//	x = 1, y = 0;
//	int64_t x1 = 0, y1 = 1, a1 = a, b1 = b;
//	while (b1) {
//		int64_t q = a1 / b1;
//		std::tie(x, x1) = std::make_tuple(x1, x - q * x1);
//		std::tie(y, y1) = std::make_tuple(y1, y - q * y1);
//		std::tie(a1, b1) = std::make_tuple(b1, a1 - q * b1);
//	}
//	return a1;
//}
//
//std::optional<uint64_t> firstMultiplier(Equation e1, Equation e2) {
//
//	uint64_t m = e2.scale;
//	uint64_t a = fancyMod(e1.scale, m);
//	uint64_t b = fancyMod(e2.offset - fancyMod(e1.offset, m) + m, m);
//
//	int64_t p, q;
//	uint64_t gcd = egcd(a, m, p, q);
//
//	if (b % gcd == 0) {
//
//		int64_t s = b / gcd * p;
//
//		if (s < 0) {
//			s += m * (-s / m + 1);
//		}
//		
//		return s;
//	}
//
//	return std::nullopt;
//}
//
//std::optional<uint64_t> validate(std::vector<int>& allZeros, uint64_t combination, int currentIndex, int previousIndex, Equation equation) {
//
//	int personCount = __popcnt64(combination);
//
//	if (currentIndex >= 0) {
//
//		// Find how many bits there were between the previous index and the current one
//
//		uint64_t m1 = (1ull << (previousIndex + 1)) - 1;
//		uint64_t m2 = (1ull << (currentIndex + 1)) - 1;
//		int bitsBetween = __popcnt64(combination & (m1 ^ m2));
//
//		Equation newEquation;
//		newEquation.scale = personCount;
//		newEquation.offset = (currentIndex > previousIndex ? bitsBetween : personCount - bitsBetween) - 1;
//		combination -= 1ull << currentIndex;
//
//		// Calculate the multiplier to get to the next valid equation, exit if none
//
//		std::optional<uint64_t> multiplier = firstMultiplier(equation, newEquation);
//		if (!multiplier.has_value()) {
//			return std::nullopt;
//		}
//		equation = {
//			.offset = equation.offset + equation.scale * multiplier.value(),
//			.scale = std::lcm(equation.scale, newEquation.scale),
//		};
//
//		// If this is the end of the chain of equations return the result
//
//		if (allZeros.size() == 0) {
//			return equation.offset + equation.scale * multiplier.value();
//		}
//	}
//
//	for (int i = 0; i < allZeros.size(); i++) {
//
//		int zeroIndex = allZeros[i];
//		std::swap(allZeros[i], allZeros.back());
//		allZeros.pop_back();
//
//		std::optional<uint64_t> possibleSolution = validate(allZeros, combination, zeroIndex, currentIndex, equation);
//		if (possibleSolution.has_value()) {
//			return possibleSolution;
//		}
//
//		allZeros.push_back(zeroIndex);
//		std::swap(allZeros[i], allZeros.back());
//	}
//
//	return std::nullopt;
//}
//
//
//
//int main() {
//
//	for (int i = 1; i < 64; i++) {
//		Magics[i] = 0xFFFFFFFFFFFFFFFF / i + 1;
//	}
//
//	std::string path = "Data/raw/" + std::to_string(N) + ".txt";
//
//	printf("Loading raw data\n");
//
//	std::ifstream file(path);
//	int count;
//	file >> count;
//	std::vector<uint64_t> candidates;
//	for (int i = 0; i < count; i++) {
//		uint64_t combination;
//		file >> combination;
//		candidates.push_back(combination);
//	}
//	file.close();
//
//	int valid = 0;
//	int invalid = 0;
//	int wrong = 0;
//
//	std::mutex mutex;
//
//	printf("Starting processing\n");
//
//	auto t1 = std::chrono::steady_clock::now();
//
//	std::vector<uint64_t> combinations;
//
//	std::for_each(std::execution::seq, candidates.begin(), candidates.end(),
//		[&](uint64_t& combination) {
//
//			std::vector<int> allZeros;
//			for (int i = 0; i < N; i++) {
//				if (((combination >> i) & 1) == 0) {
//					allZeros.push_back(i);
//				}
//			}
//
//			uint64_t combinationFull = (1ull << N) - 1;
//			Equation equation = { .offset = 0, .scale = 1 };
//			std::optional<uint64_t> result = validate(allZeros, combinationFull, -1, -1, equation);
//
//			bool hasSolution = result.has_value();
//			bool isSolutionValid = hasSolution ? Utility::ValidateSolution(combination, result.value(), N) : false;
//			
//			std::lock_guard guard(mutex);
//
//			if (hasSolution && isSolutionValid) {
//				invalid++;
//			}
//			else if (hasSolution && !isSolutionValid) {
//				wrong++;
//			}
//			else {
//				if (combination != (1ull << N) - 1) {
//					valid++;
//					combinations.push_back(combination);
//				}
//			}
//
//			printf("(%d/%d/%d/%lld) %6.2f %%\r", valid, invalid, wrong, candidates.size(), (valid + invalid) * 100.0f / candidates.size());
//		}
//	);
//
//	auto t2 = std::chrono::steady_clock::now();
//
//	printf("(%d/%d/%d/%lld) %6.2f %%\n", valid, invalid, wrong, candidates.size(), (valid + invalid) * 100.0f / candidates.size());
//
//	printf("Processing done\n");
//	printf("Found %lld valid combinations\n", combinations.size());
//
//	std::ofstream oFile("Data/filtered/" + std::to_string(N) + ".txt");
//	oFile << combinations.size() << "\n";
//	for (uint64_t combination : combinations) {
//		oFile << std::bitset<N>(combination).to_string() << "\n";
//	}
//	oFile.close();
//
//	printf("%f s\n", (t2 - t1).count() / 1e9);
//
//	return 0;
//}
//
//
//
//
//
//
