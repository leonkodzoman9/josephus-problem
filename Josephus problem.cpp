//#include <iostream>
//#include <vector>
//#include <array>
//#include <thread>
//#include <chrono>
//#include <cmath>
//#include <execution>
//
//#include <intrin.h>
//#include <immintrin.h>
//
//
//
//constexpr uint64_t ipow(uint64_t n, uint64_t p) {
//
//	uint64_t result = 1;
//	for (int i = 0; i < p; i++) {
//		result *= n;
//	}
//
//	return result;
//}
//
//
//
//constexpr uint8_t N = 24;
//constexpr uint8_t ThreadCount = 1;
//
//constexpr uint64_t UpperBound = ipow(2.0, int(1.1 * N + 1.0));
//
//constexpr uint64_t CombinationCount = 1ull << N;
//
//using Type = uint8_t;
//
//
//
//std::array<uint8_t, 64> startingIndices{};
//std::array<uint64_t, 64> magicNumbers{};
//
//
//
//void josephus(std::vector<Type>& combinations, int64_t threadID) {
//
//	std::array<uint8_t, 128> positionIndices{};
//
//	for (uint64_t q = threadID; q <= UpperBound; q += ThreadCount) {
//
//		_mm256_storeu_si256((__m256i*)(positionIndices.data() + 0), _mm256_loadu_si256((__m256i*)(startingIndices.data() + 0)));
//		_mm256_storeu_si256((__m256i*)(positionIndices.data() + 32), _mm256_loadu_si256((__m256i*)(startingIndices.data() + 32)));
//		
//		uint64_t index = 0;
//		uint64_t combination = (1ull << N) - 1;
//		for (uint64_t friendCount = N; friendCount > 0; friendCount--) {
//
//			const uint64_t low = (index + q) * magicNumbers[friendCount];
//			_mulx_u64(low, friendCount, &index);
//
//			_bittestandcomplement64((int64_t*)&combination, positionIndices[index]);
//
//			_mm256_storeu_si256((__m256i*)(positionIndices.data() + index + 0), _mm256_loadu_si256((__m256i*)(positionIndices.data() + index + 1 + 0)));
//			_mm256_storeu_si256((__m256i*)(positionIndices.data() + index + 32), _mm256_loadu_si256((__m256i*)(positionIndices.data() + index + 1 + 32)));
//			
//			combinations[combination] = 1;
//		}
//	}
//}
//
//
//
//
//
//uint64_t removeBit(uint64_t combination, int index) {
//	return _bextr_u64(combination, 0, index) | (_bextr_u64(combination, index, 64) << (index - 1));
//}
//
//bool checkCombinationForQ(uint64_t combination, uint64_t q, int maxFriends) {
//
//	uint64_t index = 0;
//	for (uint64_t friendCount = N; friendCount > maxFriends; friendCount--) {
//
//		const uint64_t low = (index + q) * magicNumbers[friendCount];
//		_mulx_u64(low, friendCount, &index);
//
//		if (_bittest64((int64_t*)&combination, index)) { return false; }
//
//		combination = removeBit(combination, index);
//	}
//
//	return true;
//}
//
//bool verifyCombination(uint64_t combination) {
//
//	const int maxFriends = _mm_popcnt_u64(combination);
//
//	for (uint64_t q = 0; q <= UpperBound; q++) {
//		if (checkCombinationForQ(combination, q, maxFriends)) {
//			return true;
//		}
//	}
//
//	return false;
//}
//
//
//
//int main() {
//
//	for (uint8_t i = 0; i < N; i++) {
//		startingIndices[i] = N - 1 - i;
//	}
//
//	for (uint8_t i = 1; i < 64; i++) {
//		magicNumbers[i] = UINT64_MAX / i + 1;
//	}
//
//	std::vector<Type> combinations(CombinationCount);
//	
//	auto t1 = std::chrono::steady_clock::now();
//	
//	std::array<std::thread, ThreadCount> threads;
//	for (uint8_t i = 0; i < ThreadCount; i++) {
//		threads[i] = std::thread(josephus, std::ref(combinations), i);
//	}
//	for (uint8_t i = 0; i < ThreadCount; i++) {
//		threads[i].join();
//	}
//	
//	auto t2 = std::chrono::steady_clock::now();
//	
//	uint64_t impossibleCount = CombinationCount - 1 - std::accumulate(combinations.begin(), combinations.end() - 1, 0ull);
//	
//	double elapsed = (t2 - t1).count() / 1000000000.0;
//	
//	printf("n is %2d, %5lld occurances, %10.3f seconds\n", N, impossibleCount, elapsed);
//	printf("%f ns/step\n", elapsed / UpperBound / N * 1000000000.0);
//
//	//auto t1 = std::chrono::steady_clock::now();
//	//auto result = verifyCombination(0b110010011);
//	//auto t2 = std::chrono::steady_clock::now();
//	//
//	//printf("%f Gq/s, %d\n", (double)UpperBound / (t2 - t1).count(), result);
//
//	return 0;
//}
//
//
//
//
//
//
