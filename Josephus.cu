//#include "cuda_runtime.h"
//#include "device_launch_parameters.h"
//
//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//
//#include <immintrin.h>
//
//
//
//constexpr uint64_t GetUpperBound(uint64_t n) {
//
//    uint64_t result = 1;
//    for (int i = 1; i <= n; i++) {
//        result = std::lcm(result, i);
//    }
//
//    return result;
//}
//
//
//
//static constexpr int N = 24;
//static constexpr int64_t UpperBound = GetUpperBound(N) / 2;
//
//static constexpr int64_t CombinationCount = 1 << N;
//static constexpr int BlockSize = 128;
//
//
//
//__global__
//void josephus(uint8_t* occurances) {
//
//    int64_t q = blockDim.x * blockIdx.x + threadIdx.x;
//
//    __shared__ uint8_t positionsLocal[BlockSize * N];
//    uint8_t* positions = positionsLocal + threadIdx.x;
//
//    for (int i = 0; i < N; i++) {
//        positions[i * BlockSize] = N - 1 - i;
//    }
//
//    __syncthreads();
//
//    uint64_t combination = CombinationCount - 1;
//    uint8_t index = 0;
//
//    for (uint8_t friendCount = N; friendCount > 0; friendCount--) {
//
//        index = (index + q) % friendCount;
//
//        combination -= 1ull << positions[index * BlockSize];
//
//        for (int i = 0; i < N - 1; i++) {
//            uint8_t i1 = positions[i * BlockSize];
//            uint8_t i2 = positions[(i + 1) * BlockSize];
//            positions[i * BlockSize] = i >= index ? i2 : i1;
//        }
//
//        occurances[combination] = 1;
//    }
//}
//
//uint64_t bitreverse(uint64_t n) {
//
//    uint64_t res = 0;
//    for (int i = 0; i < 64; i++) {
//        res = (res << 1) | (n & 1);
//        n >>= 1;
//    }
//
//    return res;
//}
//
//
//
//
//int main() {
//
//    uint8_t* occurances;
//    cudaMallocManaged(&occurances, CombinationCount * sizeof(uint8_t));
//    for (int i = 0; i < CombinationCount; i++) {
//        occurances[i] = 0;
//    }
//
//    auto t1 = std::chrono::steady_clock::now();
//
//    dim3 threads(BlockSize);
//    dim3 blocks(UpperBound / threads.x + 1);
//    josephus << <blocks, threads >> > (occurances);
//    cudaDeviceSynchronize();
//
//    auto t2 = std::chrono::steady_clock::now();
//
//
//
//    std::array<int, N> counts;
//    counts.fill(0);
//    
//    int count = 0;
//    for (int64_t i = 0; i < CombinationCount - 1; i++) {
//        if (occurances[i]) {
//            occurances[bitreverse(i) >> (64 - N)] = 1;
//        }
//        count += !occurances[i];
//        if (!occurances[i]) {
//            counts[_mm_popcnt_u64(i)]++;
//        }
//    }
//    
//    printf("N: %d, Count: %d, Time: %f s\n", N, count, (t2 - t1).count() / 1e9);
//    printf("%f M iters/s\n", (threads.x * blocks.x * (N - 15) / 1e6) / ((t2 - t1).count() / 1e9));
//    
//    for (int i = 0; i < N; i++) {
//        if (counts[i] == 0) { continue; }
//        printf("%2d: %d\n", N - i, counts[i]);
//    }
//
//    return 0;
//}
//
//
//
//
//
//
