//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//
//
//
//constexpr uint64_t lcmUpTo(uint64_t x) {
//
//	uint64_t lcm = 1;
//	for (int i = 1; i <= x; i++) {
//		lcm = std::lcm(lcm, i);
//	}
//
//	return lcm;
//}
//
//static constexpr int N = 21;
//static constexpr int64_t UpperBound = lcmUpTo(N);
//
//static constexpr int64_t CombinationCount = 1 << N;
//
//
//
//void josephus(std::vector<uint8_t>& occurances) {
//
//	std::array<uint8_t, 64> starts{};
//	for (int64_t i = 0; i < starts.size(); i++) {
//		starts[i] = N - 1 - i;
//	}
//
//	std::array<uint8_t, 64> positions{};
//
//	for (int64_t q = 0; q <= UpperBound; q++) {
//
//		std::memcpy(positions.data(), starts.data(), 64);
//
//		uint64_t combination = CombinationCount - 1;
//		int8_t index = 0;
//
//		for (int friendCount = N; friendCount > 0; friendCount--) {
//
//			index = (index + q) % friendCount;
//
//			combination -= (1ull << positions[index]);
//
//			uint8_t* indexToDelete = positions.data() + index;
//			std::memcpy(indexToDelete, indexToDelete + 1, 63 - index);
//			
//			occurances[combination] = 1;
//		}
//	}
//}
//
//int main() {
//
//	std::vector<uint8_t> occurances(CombinationCount);
//
//	auto t1 = std::chrono::steady_clock::now();
//	josephus(occurances);
//	auto t2 = std::chrono::steady_clock::now();
//
//	int count = 0;
//	for (int64_t i = 0; i < CombinationCount - 1; i++) {
//		count += !occurances[i];
//	}
//
//	printf("N: %d, Count: %d, Time: %f s\n", N, count, (t2 - t1).count() / 1e9);
//	printf("%f ns/iter\n", (t2 - t1).count() / double(UpperBound * N));
//
//	return 0;
//}
//
//
//
//
//
//
