//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//#include <algorithm>
//#include <execution>
//#include <bitset>
//#include <numeric>
//#include <execution>
//
//#include <ranges>
//
//#include <immintrin.h>
//
//
//
//constexpr uint64_t lcmUpTo(uint64_t x) {
//
//	uint64_t lcm = 1;
//	for (int i = 1; i <= x; i++) {
//		lcm = std::lcm(lcm, i);
//	}
//
//	return lcm;
//}
//
//static constexpr int N = 21;
//static constexpr int64_t UpperBound = lcmUpTo(N);
//
//static constexpr int64_t CombinationCount = 1 << N;
//
//
//
//void josephus(std::vector<bool>& occurances) {
//
//	std::array<uint64_t, 64> magics;
//	for (int i = 1; i < 64; i++) {
//		magics[i] = 0xFFFFFFFFFFFFFFFF / i + 1;
//	}
//
//	int64_t combinationIndex = 0;
//	std::vector<uint64_t> combinations;
//	combinations.resize(1024 * 1024 * N);
//
//	for (int64_t q = 0; q < UpperBound; q++) {
//
//		uint64_t combination = CombinationCount - 1;
//		uint64_t index = 0;
//
//		for (int friendCount = N; friendCount > 0; friendCount--) {
//			_mulx_u64((index + q) * magics[friendCount], friendCount, &index);
//			combination -= _pdep_u64(1 << index, combination);
//			combinations[combinationIndex++] = combination;
//		}
//
//		if (combinationIndex >= 1024 * 1024 * N) {
//			for (uint64_t combination : combinations) {
//				occurances[combination] = true;
//			}
//			combinationIndex = 0;
//		}
//	}
//
//	for (uint64_t combination : combinations) {
//		occurances[combination] = true;
//	}
//}
//
//int main() {
//
//	std::vector<bool> occurances(CombinationCount);
//
//	auto t1 = std::chrono::steady_clock::now();
//	josephus(occurances);
//	auto t2 = std::chrono::steady_clock::now();
//
//	std::array<int, N> counts;
//	counts.fill(0);
//	
//	int count = 0;
//	for (int64_t i = 0; i < CombinationCount - 1; i++) {
//		count += !occurances[i];
//		if (!occurances[i]) {
//			counts[_mm_popcnt_u64(i)]++;
//		}
//	}
//
//	printf("N: %d, Count: %d, Time: %f s\n", N, count, (t2 - t1).count() / 1e9);
//	printf("%f ns/q\n", (t2 - t1).count() / double(UpperBound));
//	printf("%f Mqs/s\n", double(UpperBound) / (t2 - t1).count() * 1e9 / 1e6);
//	
//	for (int i = 0; i < N; i++) {
//		if (counts[i] == 0) { continue; }
//		printf("%2d: %d\n", N - i, counts[i]);
//	}
//
//	return 0;
//}
//
//
//
//
//
//
