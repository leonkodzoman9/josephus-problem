//#include <iostream>	
//#include <vector>
//#include <array>
//#include <chrono>
//#include <numeric>
//#include <algorithm>
//#include <execution>
//#include <bitset>
//#include <numeric>
//#include <execution>
//
//#include <immintrin.h>
//
//
//
//constexpr uint64_t lcmUpTo(uint64_t x) {
//
//	uint64_t lcm = 1;
//	for (int i = 1; i <= x; i++) {
//		lcm = std::lcm(lcm, i);
//	}
//
//	return lcm;
//}
//
//static constexpr int N = 21;
//static constexpr int64_t UpperBound = lcmUpTo(N);
//
//static constexpr int64_t CombinationCount = 1 << N;
//
//
//
//void josephus(std::vector<uint8_t>& occurances) {
//
//	const __m512i _0 = _mm512_setzero_si512();
//	const __m512i _1 = _mm512_set1_epi64(1);
//	const __m512i _neg1 = _mm512_set1_epi64(-1);
//	const __m512i _8 = _mm512_set1_epi64(8);
//
//	__m512i _starts;
//	for (int64_t i = 0; i < 64; i++) {
//		_starts.m512i_i8[i] = N - 1 - i;
//	}
//
//	std::array<uint64_t, 64> magics{};
//	for (int64_t i = 1; i < 64; i++) {
//		magics[i] = 0xFFFFFFFFFFFFF / i + 1;
//	}
//
//	__m512i _qs;
//	for (int i = 0; i < 8; i++) {
//		_qs.m512i_u64[i] = i;
//	}
//
//	std::array<uint64_t, 64> oneBits;
//	std::array<__mmask64, 64> zeroBits;
//	for (int i = 0; i < 64; i++) {
//		oneBits[i] = 1ull << i;
//		zeroBits[i] = -1 ^ (1ull << i);
//	}
//
//	for (int64_t q = 0; q <= UpperBound; q += 8) {
//
//		std::array<__m512i, 8> _positionsArrays{};
//		for (int i = 0; i < 8; i++) {
//			_positionsArrays[i] = _starts;
//		}
//
//		__m512i _combinations = _mm512_set1_epi64(CombinationCount - 1);
//
//		__m512i _indices = _mm512_setzero_si512();
//		__m512i _friendCount = _mm512_set1_epi64(N);
//		for (int friendCount = N; friendCount > 0; friendCount--) {
//
//			__m512i _magic = _mm512_set1_epi64(magics[friendCount]);
//			__m512i _is = _mm512_madd52lo_epu64(_0, _mm512_add_epi64(_indices, _qs), _magic);
//			_indices = _mm512_madd52hi_epu64(_0, _is, _friendCount);
//
//			uint64_t indices[8];
//			_mm512_storeu_epi64(indices, _indices);
//
//			for (int i = 0; i < 8; i++) {
//				_combinations.m512i_u64[i] -=oneBits[_positionsArrays[i].m512i_i8[indices[i]]];
//			}
//
//			for (int i = 0; i < 8; i++) {
//				_positionsArrays[i] = _mm512_mask_compress_epi8(_0, zeroBits[indices[i]], _positionsArrays[i]);
//			}
//
//			for (int i = 0; i < 8; i++) {
//				occurances[_combinations.m512i_u64[i]] = 1;
//			}
//
//			_friendCount = _mm512_sub_epi64(_friendCount, _1);
//		}
//
//		_qs = _mm512_add_epi64(_qs, _8);
//	}
//}
//
//int main() {
//
//	std::vector<uint8_t> occurances(CombinationCount);
//
//	auto t1 = std::chrono::steady_clock::now();
//	josephus(occurances);
//	auto t2 = std::chrono::steady_clock::now();
//
//	std::array<int, N> counts;
//	counts.fill(0);
//
//	int count = 0;
//	for (int64_t i = 0; i < CombinationCount - 1; i++) {
//		count += !occurances[i];
//		if (!occurances[i]) {
//			counts[_mm_popcnt_u64(i)]++;
//		}
//	}
//
//	printf("N: %d, Count: %d, Time: %f s\n", N, count, (t2 - t1).count() / 1e9);
//	printf("%f ns/iter\n", (t2 - t1).count() / double(UpperBound * N));
//	printf("%f Mqs/s\n", double(UpperBound) / (t2 - t1).count() * 1e9 / 1e6);
//
//	for (int i = 0; i < N; i++) {
//		if (counts[i] == 0) { continue; }
//		printf("%2d: %d\n", N - i, counts[i]);
//	}
//
//	return 0;
//}
//
//
//
//
//
//
//
//
//
//
